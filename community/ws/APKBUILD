# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Leo <thinkabit.ukim@gmail.com>
pkgname=ws
pkgver=7.8.5
pkgrel=0
pkgdesc="websocket and http client and server command line swiss army"
options="!check"
url="https://github.com/machinezone/IXWebSocket"
arch="all"
license="BSD-3-Clause"
makedepends="cmake openssl-dev zlib-dev jsoncpp-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/machinezone/IXWebSocket/archive/v$pkgver.tar.gz"
builddir="$srcdir/IXWebSocket-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DUSE_TLS=1 \
		-DUSE_WS=1 \
		${CMAKE_CROSSOPTS} ..
	make
}

package() {
	make -C build DESTDIR="$pkgdir" install

	# We don't care about anything but the ws binary
	rm -rf "$pkgdir"/usr/include
	rm -rf "$pkgdir"/usr/lib
}
sha512sums="85fc2b9d350c1b0d68723f51e3261b80543f4a9e71c7c0a7b24663a99c88a4dca524b24a314e1c132b52bdeec5f05f5111329e5ef15c7ad2a2626201df5ec99e  ws-7.8.5.tar.gz"
